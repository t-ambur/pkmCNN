import cv2
import tensorflow as tf
import numpy as np
import os
#
import constants as c


def prepare_tile(filepath: str):
    IMG_SIZE = c.TILE_SIZE_CNN
    img_array = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
    img_array = img_array/255.0
    new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    return new_array.reshape(-1, IMG_SIZE, IMG_SIZE, 1)


def predict_tile(filepath: str, grid_model: tf.keras.Model, verbose=0):
    # keras < x.5
    # prediction = model.predict_classes([prepare(filepath)])
    # index = prediction[0][0]
    # return index

    # newer keras
    predict_x = grid_model.predict([prepare_tile(filepath)], verbose=verbose)
    # print('predicx x:', predict_x, flush=True)
    classes_x = np.argmax(predict_x, axis=1)
    # print("classes x:", classes_x, flush=True)
    return classes_x[0]


# bulk prediction
def predict_tiles_in_dir(directory: list, grid_model: tf.keras.Model, labels: list, verbose=0):
    prediction_dict = dict()
    batch_to_predict = []
    tile_names = []
    for file in os.listdir(directory):
        file = str(file)
        if (file == 'readme.txt'):
            continue
        batch_to_predict.append(
            prepare_tile(os.path.join(directory, file))
        )
        tile_names.append(
            file[file.find('y_') : file.find('.png')].replace('_', '')
        )
    predict_x = grid_model.predict(
        np.vstack(batch_to_predict),
        verbose=verbose,
        workers=4,
        batch_size=20
    )
    classes_x = np.argmax(predict_x, axis=1)
    # print(classes_x)
    for idx, tile_name in enumerate(tile_names):
        # y_x_
        # prediction_dict[tile_name] = labels[classes_x[idx]]
        # x_y_
        prediction_dict[
            tile_name[2:4] + tile_name[0:2]
            ] = labels[classes_x[idx]]
    return prediction_dict


'''
# sequential
def predict_tiles_in_dir(directory: list, grid_model: tf.keras.Model, labels: list):
    prediction_dict = dict()
    for file in os.listdir(directory):
        file = str(file)
        if (file == 'readme.txt'):
            continue
        try:
            # prepare_tile(filepath)
            tile_index = predict_tile(os.path.join(directory, file), grid_model)
            tile_name = file[file.find('y_') : file.find('.png')]
            # print('tile_name:', tile_name)
            prediction_dict[tile_name] = labels[tile_index]
        except Exception as e:
            print('ERROR: Exception caught while trying to predict tile image:', file)
            tile = None
            tile_name = "Unknown"
    return prediction_dict
'''
