import os

EMULATOR_DIR = os.path.join(".", "emulator")
EMULATOR_LOCATION = os.path.join(EMULATOR_DIR, "visualboyadvance-m.exe")
SAVED_GAME_FILE_LOCATION = os.path.join(EMULATOR_DIR, "blue-0#.sgm")
BLUE_GAME_DATA_FILE = os.path.join(EMULATOR_DIR, 'blue.sav')
GOAL_SAVE_LOCATION = os.path.join(EMULATOR_DIR, "goal-sb#.pkl")
EMULATOR_NAME = "VisualBoyAdvance-M 2.1.4"
EMULATOR_NAME_RUNNING = "blue - VisualBoyAdvance-M 2.1.4"
ROM_NAME = "blue.zip"

EMULATOR_BOOT_DELAY = 6
CINEMATIC_DELAY = False

# SCREEN_SHOT_Y_REMOVAL_SIZE_3 = 45
SCREEN_SHOT_SIDES_REMOVAL_SIZE_4 = 20 # x needs to be 1600
SCREEN_SHOT_TOP_REMOVAL_SIZE_4 = 108 + SCREEN_SHOT_SIDES_REMOVAL_SIZE_4 # y needs to be 1440

# SCREEN_SHOT_INTERVAL = .2
DELAY_AFTER_MOVEMENT = 0.15 # SECONDS

TILE_SIZE_CROPPED = 160
TILE_SIZE_CNN = 80
PLAYER_GRID = 'x4y4'
TEXTBOX_GRID = 'x0y6'

X_GRIDS_NAME = "X_grids.pkl"
Y_GRIDS_NAME = "y_grids.pkl"
METADATA_GRIDS_NAME = 'metadata_grids.pkl'
IMAGE_OUTPUT_DIR = os.path.join('.', 'output')
GRIDS_CNN_PATH = os.path.join("train", "data", "grids")
GRIDS_MODEL_PATH = os.path.join(GRIDS_CNN_PATH, "GridsCNN")
GRIDS_OUTPUT_PATH = os.path.join(IMAGE_OUTPUT_DIR, "grids")

DIRECTIONS = ['up', 'down', 'left', 'right']
GOAL_STAGE_KEY = 'stage'
GOALS_KEY = 'goalTiles'
BLOCK_GOAL_KEY = 'block_tile'
GOAL_COUNTER = 'counter'

# BTL_PATH = "train\\data\\battle-or-not"
# BTL_MODEL_PATH = "train\\data\\battle-or-not\\CNN1.model"
# TEXT_PATH = "train\\data\\text-or-not"
# TEXT_MODEL_PATH = "train\\data\\text-or-not\\CNN2.model"

# BTL_CATEGORIES = ["nonbattle", "battle"]
# TEXT_CATEGORIES = ["nontext", "text"]
