import time
import random
#
import keyInjector as KI

KEYPRESS_DELAY = .05


class Control:
    def __init__(self, ahk):
        self.ahk = ahk
        self.print = False

    # random focused on moving around
    def random_fast(self):
        rand = random.randint(1, 5)
        if rand <= 1:
            self.press_a()
        elif rand == 2:
            self.press_b()
        else:
            self.rand_direction(2)

    def random_world(self):
        rand = random.randint(1, 4)
        if rand <= 1:
            self.press_a()
        else:
            self.rand_direction(2)

    def random_available_dir_world(self, available_dict: dict):
        avail_dir = []
        for key, value in available_dict.items():
            if value == True:
                avail_dir.append(key)
        if len(avail_dir) <= 0:
            return
        self.press_str_direction(
            avail_dir[random.randint(0, len(avail_dir)-1)]
        )

    def press_str_direction(self, str_direction, times=2):
        for i in range(0, times, 1):
            match str_direction:
                case "up":
                    self.press_up()
                case "left":
                    self.press_left()
                case "down":
                    self.press_down()
                case "right":
                    self.press_right()
                case _:
                    self.press_b()

    # random focused on battle and text boxes
    def rand_sequence(self):
        rand = random.randint(1, 2)
        if rand <= 1:
            self.press_a()
            self.rand_direction(1)
        else:
            self.press_b()
            self.rand_direction(1)

    # sends a random direction from the keyboard
    def rand_direction(self, times):
        rand = random.randint(1, 4)
        if rand <= 1:
            for i in range(0, times, 1):
                self.press_down()
        elif rand == 2:
            for i in range(0, times, 1):
                self.press_up()
        elif rand == 3:
            for i in range(0, times, 1):
                self.press_left()
        else:
            for i in range(0, times, 1):
                self.press_right()

    # sends a random number downs
    def rand_down(self):
        rand = random.randint(1, 3)
        for i in range(0, rand, 1):
            self.press_down()

    def rand_up(self):
        rand = random.randint(1, 3)
        for i in range(0, rand, 1):
            self.press_up()

    def rand_left(self):
        rand = random.randint(1, 3)
        for i in range(0, rand, 1):
            self.press_left()

    # sends a random number of A's
    def rand_a(self, m):
        rand = random.randint(1, m)
        for i in range(0, rand, 1):
            self.press_a()

    def rand_b(self, m):
        rand = random.randint(1, m)
        for i in range(0, rand, 1):
            self.press_b()

    def battle_sequence(self):
        self.press_a()
        self.rand_up()
        self.rand_left()
        # self.rand_a(10)

    def clear_text_sequence(self):
        self.press_b()

    def no_interactions(self):
        self.rand_direction(2)

    def press_a(self, times=1):
        for i in range(0, times, 1):
            KI.KeyPress(KI.A, KEYPRESS_DELAY)
            if self.print:
                print("pressed A", flush=True)
            time.sleep(0.2)

    def press_b(self, times=1):
        for i in range(0, times, 1):
            KI.KeyPress(KI.B, KEYPRESS_DELAY)
            if self.print:
                print("pressed B", flush=True)
            time.sleep(0.2)

    def press_down(self):
        KI.KeyPress(KI.DOWN, KEYPRESS_DELAY)

    def press_up(self):
        KI.KeyPress(KI.UP, KEYPRESS_DELAY)

    def press_left(self):
        KI.KeyPress(KI.LEFT, KEYPRESS_DELAY)

    def press_right(self):
        KI.KeyPress(KI.RIGHT, KEYPRESS_DELAY)

    def save_game(self, game_state_bank):
        if (game_state_bank <= 1):
            KI.TwoKeyPress(KI.SHIFT_KEY_L, KI.F1_KEY, KEYPRESS_DELAY)
        elif (game_state_bank == 2):
            KI.TwoKeyPress(KI.SHIFT_KEY_L, KI.F2_KEY, KEYPRESS_DELAY)
        elif (game_state_bank == 3):
            KI.TwoKeyPress(KI.SHIFT_KEY_L, KI.F3_KEY, KEYPRESS_DELAY)
        elif (game_state_bank == 4):
            KI.TwoKeyPress(KI.SHIFT_KEY_L, KI.F4_KEY, KEYPRESS_DELAY)
        else:
            KI.TwoKeyPress(KI.SHIFT_KEY_L, KI.F5_KEY, KEYPRESS_DELAY)
