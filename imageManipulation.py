from PIL import Image
import os
#
import constants as c


def crop(image: Image.Image, grids_dir: str, frame_counter: int, recording_time=-0.1):
    # get width and height
    # print('image dimesions:', str(image.size))
    w, h = image.size # w = 1600, h = 1440
    # size of the cropped 'tiles'
    w_size = c.TILE_SIZE_CROPPED # (32*5) not sure why, but the scaling that VBA-M uses seems to be one times less in the GUI than it is for this calculation
    h_size = w_size
    output_dir = os.path.join(grids_dir, str(frame_counter))

    for y in range(0, h, h_size):
        for x in range(0, w, w_size):
            # print('cropping x:', x, ' y:', y, ' xlimit:', x+w_size, ' ylimit:', y+h_size)
            tile: Image.Image = image.crop((x, y, x+w_size, y+h_size))

            if recording_time < 0:
                filename = "frame_" + str(frame_counter) + "_grid_y_" + str(int(y/h_size)) + "_x_" + str(int(x/w_size)) + ".png"
            else:
                filename = "record_time_" + str(recording_time) + "_grid_y_" + str(int(y/h_size)) + "_x_" + str(int(x/w_size)) + ".png"
            
            tile.save(os.path.join(output_dir, filename))
        # end inner loop
    # end outer loop

# clear out the old images from the grids folder!


'''
each cell is xy
the grid screenshots save as yx

+----+----+----+----+----+----+----+----+----+----+
| 00 | 10 | 20 | 30 | 40 | 50 | 60 | 70 | 80 | 90 |
+----+----+----+----+----+----+----+----+----+----+
| 01 | 11 | 21 | 31 | 41 | 51 | 61 | 71 | 81 | 91 |
+----+----+----+----+----+----+----+----+----+----+
| 02 | 12 | 22 | 32 | 42 | 52 | 62 | 72 | 82 | 92 |
+----+----+----+----+----+----+----+----+----+----+
| 03 | 13 | 23 | 33 | 43 | 53 | 63 | 73 | 83 | 93 |
+----+----+----+----+----+----+----+----+----+----+
| 04 | 14 | 24 | 34 | 44 | 54 | 64 | 74 | 84 | 94 |
+----+----+----+----+----+----+----+----+----+----+
| 05 | 15 | 25 | 35 | 45 | 55 | 65 | 75 | 85 | 95 |
+----+----+----+----+----+----+----+----+----+----+
| 06 | 16 | 26 | 36 | 46 | 56 | 66 | 76 | 86 | 96 |
+----+----+----+----+----+----+----+----+----+----+
| 07 | 17 | 27 | 37 | 47 | 57 | 67 | 77 | 87 | 97 |
+----+----+----+----+----+----+----+----+----+----+
| 08 | 18 | 28 | 38 | 48 | 58 | 68 | 78 | 88 | 98 |
+----+----+----+----+----+----+----+----+----+----+

44 has the character
frame_6_grid_y_4_x_4.png is an example file name
                    +----+
                    | 43 |
               +----+----+----+
               | 34 | 44 | 54 | 
               +----+----+----+
                    | 45 |
                    +----+

Note there are 4 tiles to the left of the character and 5 to the right
Due to the ratio of the original gameboy screen? 1.1:1 (w:h)

from skimage import img_as_float
from skimage.measure import compare_mse as mse
'''

# from skimage import img_as_float
# from skimage.metrics import mean_squared_error
# from skimage.metrics import structural_similarity
# def mse(image1: Image.Image, image2: Image.Image):
#     return mean_squared_error(img_as_float(image1), img_as_float(image2))
# def ssim(image1: Image.Image, image2: Image.Image):
#     return structural_similarity(img_as_float(image1), img_as_float(image2))
