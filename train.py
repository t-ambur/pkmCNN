from tensorflow.python.keras.engine.sequential import Sequential
from tensorflow.python.keras.layers import Dense, Activation, Flatten, Conv2D, MaxPooling2D # Dropout was here but not used
from keras.utils import to_categorical
# from _ import TensorBoard
import time
import numpy as np
import constants as c
import os
#
from util_fn import load_from_pkl


def load_pickles(path, X_name, y_name, metadata_name):
    X = load_from_pkl(os.path.join(path, X_name))
    y = load_from_pkl(os.path.join(path, y_name))
    metadata = load_from_pkl(os.path.join(path, metadata_name))
    return X, y, metadata


def train(X, y, dense_layers, layer_sizes, conv_layers, output_layer_size, batch_size=32, epochs=10, validation_split=0.3):
    model = None
    print("building network...", flush=True)
    for dense_layer in dense_layers:
        for layer_size in layer_sizes:
            for conv_layer in conv_layers:
                NAME = "{}-conv-{}-nodes-{}-dense-{}".format(conv_layer, layer_size, dense_layer, int(time.time()))
                print(NAME)

                model = Sequential()

                model.add(Conv2D(layer_size, (3, 3), input_shape=X.shape[1:]))
                model.add(Activation('relu'))
                model.add(MaxPooling2D(pool_size=(2, 2)))

                for l in range(conv_layer-1):
                    model.add(Conv2D(layer_size, (3, 3)))
                    model.add(Activation('relu'))
                    model.add(MaxPooling2D(pool_size=(2, 2)))

                model.add(Flatten())

                for _ in range(dense_layer):
                    model.add(Dense(layer_size))
                    model.add(Activation('relu'))

                model.add(Dense(output_layer_size, activation='softmax')) # size 1 and sigmoid for binary classifiction

                # tensorboard = TensorBoard(log_dir="logs\\{}".format(NAME))
                print("Compiling model...", flush=True)
                model.compile(loss='binary_crossentropy',
                              optimizer='adam',
                              metrics=['accuracy'],
                              )

                y = np.asarray(y)
                print("Start training", flush=True)
                model.fit(X,
                          to_categorical(y), # need to convert array of integers into one-hot encoding for categorical (just 'y' for binary classification)
                          batch_size=batch_size,
                          epochs=epochs,
                          validation_split=validation_split)
    # end of training loop
    return model


if __name__ == "__main__":
    # CNN not yet tweaked
    dense_layers = [0]
    layer_sizes = [32] # 64 did .0005 better than 0 32 0 (.9996 train, 1.0 validate)
    conv_layers = [0]

    print("loading pickle files...", flush=True)
    data, labels, metadata = load_pickles(c.GRIDS_CNN_PATH, c.X_GRIDS_NAME, c.Y_GRIDS_NAME, c.METADATA_GRIDS_NAME)
    print(metadata)

    print("training grids model...", flush=True)
    model = train(data, labels, dense_layers, layer_sizes, conv_layers, len(metadata["all_labels"]), batch_size=128, epochs=3, validation_split=0.2)

    print("saving model...", flush=True)
    save_location = c.GRIDS_MODEL_PATH
    model.save(save_location)

    print("Complete. Model saved to:", save_location)
    print("\n\n", flush=True)


# binary classification

# CNN1.model battle/nonbattle
# 3 EPOCHS
# print("training battle vs nonbattle model...", flush=True)
# dense_layers = [0]
# layer_sizes = [32]
# conv_layers = [1]
# print("loading pickle files...", flush=True)
# data, labels = load_pickles(c.BTL_PATH)
# model = train(data, labels, dense_layers, layer_sizes, conv_layers)
# print("saving model...", flush=True)
# save_location = c.BTL_MODEL_PATH
# model.save(save_location)
# print("Complete. Model saved to:", save_location)
# print("\n\n", flush=True)

# CNN2.model text/nontext
# EPOCHS untested, using 3
# print("training text vs nontext model...", flush=True)
# dense_layers = [0]
# layer_sizes = [32]
# conv_layers = [1]
# print("loading pickle files...", flush=True)
# data, labels = load_pickles(c.TEXT_PATH)
# model = train(data, labels, dense_layers, layer_sizes, conv_layers)
# print("saving model...", flush=True)
# save_location = c.TEXT_MODEL_PATH
# model.save(save_location)
# print("Complete. Model saved to:", save_location)
# print("\n\n", flush=True)
'''
    for dense_layer in dense_layers:
        for layer_size in layer_sizes:
            for conv_layer in conv_layers:
                NAME = "{}-conv-{}-nodes-{}-dense-{}".format(conv_layer, layer_size, dense_layer, int(time.time()))
                print(NAME)

                model = Sequential()

                model.add(Conv2D(layer_size, (3, 3), input_shape=X.shape[1:]))
                model.add(Activation('relu'))
                model.add(MaxPooling2D(pool_size=(2, 2)))

                for l in range(conv_layer-1):
                    model.add(Conv2D(layer_size, (3, 3)))
                    model.add(Activation('relu'))
                    model.add(MaxPooling2D(pool_size=(2, 2)))

                model.add(Flatten())

                for _ in range(dense_layer):
                    model.add(Dense(layer_size))
                    model.add(Activation('relu'))

                model.add(Dense(1))
                model.add(Activation('sigmoid'))

                # tensorboard = TensorBoard(log_dir="logs\\{}".format(NAME))
                print("Compiling model...", flush=True)
                model.compile(loss='binary_crossentropy',
                              optimizer='adam',
                              metrics=['accuracy'],
                              )

                y = np.asarray(y)
                print("Start training", flush=True)
                model.fit(X, y,
                          batch_size=batch_size,
                          epochs=epochs,
                          validation_split=validation_split)
'''