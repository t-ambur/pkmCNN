

-----Software Required-----
>> python 3.10
>> python modules: see/install requirements.txt

>> AutoHotkey.exe installed (to default install location OR PATH configured properly)

>> VisualBoyAdvance-M (version 2.1.4) emulator (SPECIFIC VERSION, ALSO MUST BE THE GITHUB M VARIANT)
If you have modified the control scheme for VBA-M, reset one of the schemes to the default keybindings

You need to change the default border setting:
    options -> game boy -> configure -> display borders: Never
And turn off the red text when saving game state:
    options -> video -> disable on screen display
And its nice to force the window to stay on top:
    options -> video -> keep window on top


>> A terminal or command line tool to execute python scripts.

------Run------
Please note the AI will take control of your keyboard input and will require you to be "hands free"
while it runs the game. It will run on its own unless you start monkeying around.
This is because this AI is a "blackbox" approach to simulate a human playing the game. The AI knows nothing
of the internal code of the game. It will use your keyboard for input and take screenshots of the game window
in order to get feedback from the game. The neural network uses the images received to make decisions on key input.

>> You will need to acquire a Pokemon blue ROM
Name the ROM: blue.zip
If your ROM is not a zip file, I can't guarantee that it will work, but you can try changing the ROM name in constants.py

Don't use an existing save, start a new game the first time. Afterward, the AI will save the 'game state' (using the emulator) every
20 seconds or so.

The AI will save with 'save states'. It is recommended that you do not save using the in-game menu, as this may interfere with
the 'new game' script.

Please refrain from resizing the window after launch, the width and height of the screen is important

Place the blue.zip ROM and the emulator in the emulator folder

In your terminal, to start a new game, Run: python pkmblue.py
To start an existing game (after you've ran a new game past the intro for at least 20 seconds), Run: python pkmblue.py -c

Enjoy!
