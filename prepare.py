import numpy as np
import os
import random
import cv2  # for image manipulation
import constants as c
#
from util_fn import save_to_pkl

GRID_RESIZE_SIZE = c.TILE_SIZE_CNN

def create_training_data(path_to_categories: str, categories: list, training_data: list, max_examples: int):
    for category in categories:
        path = os.path.join(path_to_categories, category)
        index = categories.index(category)
        current_category_data = []
        for img in os.listdir(path):
            try:
                img_array = cv2.imread(os.path.join(path, img),
                                       cv2.IMREAD_GRAYSCALE)  # convert to array, convert to greyscale
                img_array = img_array/255  # normalize
                new_array = cv2.resize(img_array, (GRID_RESIZE_SIZE, GRID_RESIZE_SIZE))  # resize
                current_category_data.append([new_array, index])
            except Exception as e:
                print(str(img), "could not preprocess", flush=True)
                pass
        # oversample the data based on the label with the most data files
        len_data_before_oversample = len(current_category_data)
        counter = 0
        while len(current_category_data) < max_examples:
            if counter >= len_data_before_oversample:
                counter = 0
            current_category_data.append(current_category_data[counter])
        training_data.extend(current_category_data)


def prep(path_to_categories: str, training_data: list, max_examples: int):
    print("preparing images...", flush=True)
    categories = os.listdir(path_to_categories)
    create_training_data(path_to_categories, categories, training_data, max_examples)
    print("shuffling data", flush=True)
    random.shuffle(training_data)
    X = []  # capital X is your feature set
    y = []  # lowercase y is your labels
    print("generating pickle files...", flush=True)
    for features, label in training_data:
        X.append(features)
        y.append(label)

    X = np.array(X).reshape(-1, GRID_RESIZE_SIZE, GRID_RESIZE_SIZE, 1)  # -1 for any number, 1 for grayscale (3 for RGB)
    return X, y


def save(X, y, location: str, training_data: list, max_examples: int, all_label_names: list):
    # training data
    save_to_pkl(os.path.join(location, c.X_GRIDS_NAME), X)
    print("X done.", flush=True)
    save_to_pkl(os.path.join(location, c.Y_GRIDS_NAME), y)
    print("Y done.", flush=True)
    training_data.clear()

    # metadata
    metadata = {
        "max_examples": max_examples,
        "all_labels": all_label_names
    }
    save_to_pkl(os.path.join(location, c.METADATA_GRIDS_NAME), metadata)
    print("Metadata done. Prepare done.")


def find_max_examples_and_all_labels(path_to_all_categories):
    max_examples = 0
    all_categories = os.listdir(path_to_all_categories)
    for folder in all_categories:
        num_examples = len( os.listdir( os.path.join(path_to_all_categories, folder) ) )
        if num_examples > max_examples:
            max_examples = num_examples
    return max_examples, all_categories


if __name__ == "__main__":
    # train holds all folders containing labels
    data_dir = os.path.join("train", "grids")
    training_data = []

    print("Preparing Grids", flush=True)
    max_examples, all_label_names = find_max_examples_and_all_labels(data_dir)
    X, y = prep(data_dir, training_data, max_examples)
    print("Length of all training data:", str(len(training_data)), flush=True)
    save(X, y, c.GRIDS_CNN_PATH, training_data, max_examples, all_label_names)
