from ahk import AHK
import time
import os
from PIL import ImageGrab
import constants as c
#
import imageManipulation as imgMan

ahk = AHK()

SCREENSHOT_INTERVAL = 2


def screenshot():
    window = ahk.active_window
    x = window.position[0]
    y = window.position[1]
    h = window.height
    w = window.width
    im = ImageGrab.grab(bbox=(
        x + c.SCREEN_SHOT_SIDES_REMOVAL_SIZE_4,
        y + c.SCREEN_SHOT_TOP_REMOVAL_SIZE_4,
        x + w - c.SCREEN_SHOT_SIDES_REMOVAL_SIZE_4,
        y + h - c.SCREEN_SHOT_SIDES_REMOVAL_SIZE_4
        ))
    t = time.time()
    imgMan.crop(im, os.path.join("output", "grids"), "record", recording_time=t)
    print("Grids Captured at time:", str(t), flush=True)


looping = True
while looping:
    screenshot()
    time.sleep(SCREENSHOT_INTERVAL)
