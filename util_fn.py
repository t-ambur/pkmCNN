import pickle
import os


def load_from_pkl(inputlocation):
    with open(inputlocation,"rb") as pickle_in:
        return pickle.load(pickle_in)


def save_to_pkl(outputlocation, data):
    with open(outputlocation, 'wb') as pickle_out:
        pickle.dump(data, pickle_out)


def cleanup_old_images(dir):
    for f in os.listdir(dir):
        if f.endswith(".png"):
            os.remove(os.path.join(dir, f))
