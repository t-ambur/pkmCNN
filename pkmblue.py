from ahk import AHK
import os
from PIL import ImageGrab
import threading
import tensorflow as tf
import time
####
import cli
import titleScreen
import controls as controls
import constants as c
import predict
import imageManipulation as imgMan
import pathing
import util_fn as u
####

# cleanup old image files
print("Removing old screenshots...", flush=True)
u.cleanup_old_images(c.IMAGE_OUTPUT_DIR)
for p in os.listdir(c.GRIDS_OUTPUT_PATH):
    if p == 'record':
        continue
    grid_folder = os.path.join(c.GRIDS_OUTPUT_PATH, p)
    if os.path.isdir(grid_folder):
        u.cleanup_old_images(grid_folder)


# AutoHotkey wrapper init
ahk = AHK()
# New game or continue?
continue_game = bool(cli.args.continueGame)
game_state_bank = int(cli.args.saveGameStateBank)
game_save_exists = False
# save files (based on current bank)
game_state_bank_location = c.SAVED_GAME_FILE_LOCATION.replace('#', str(game_state_bank))
goal_save_location = c.GOAL_SAVE_LOCATION.replace('#', str(game_state_bank))
if (os.path.exists(game_state_bank_location)):
    # remove the goal file if we are in debug mode (use a hardcoded one)
    if bool(cli.args.debugMode) and continue_game:
        if os.path.exists(goal_save_location):
            os.remove(goal_save_location)
    game_save_exists = True

if continue_game and game_save_exists:
    print("AI will continue from saved game...", flush=True)
elif continue_game and not game_save_exists:
    print("AI will start a new game (there is no save file to continue from)...", flush=True)
else:
    print("AI will start a new game...", flush=True)

# delete the 'settings' game data in order to generate a new file
# the .sgm file doesn't exist, so neither should the .sav file and .goal file
# or we want to start a new game on this bank, so we need to delete the old files
if not game_save_exists or not continue_game: 
    # delete the 'settings' .sav file if starting a new game
    if os.path.exists(c.BLUE_GAME_DATA_FILE):
        print('Removing old "settings .sav" file...', flush=True)
        os.remove(c.BLUE_GAME_DATA_FILE)
    # delete the goal progress if starting a new game on this bank
    if os.path.exists(goal_save_location):
        print('Removing old goal...', flush=True)
        os.remove(goal_save_location) # Didn't delete TODO

# delete the existing save if starting a new game (.sgm)
if not continue_game and game_save_exists:
    os.remove(game_state_bank_location)

# grab the AI's 'goal dictionary' (contains a list of target tiles)
goal_dict = pathing.load_goal_file( goal_save_location )

title = titleScreen.StartGame(ahk)
title.insert_card()
if continue_game and game_save_exists:
    title.continue_game(game_state_bank)
else: # new game
    title.get_to_title_window()
    title.titleOptions()
    title.startNewGame(False)

control = controls.Control(ahk)
counter = 1

print("Loading labels...", flush=True)
GRIDS_METADATA = u.load_from_pkl(os.path.join(c.GRIDS_CNN_PATH, c.METADATA_GRIDS_NAME))
GRIDS_LABELS = GRIDS_METADATA['all_labels']

print("Loading CNNs...", flush=True)
GRIDS_MODEL = tf.keras.models.load_model(c.GRIDS_MODEL_PATH)


def create_daemon():
    thread = threading.Thread(name='daemon_screenshot', target=screenshot)
    thread.daemon = True
    return thread


def screenshot():
    global counter
    if counter > 19:
        counter = 0
        control.save_game(game_state_bank)
        u.save_to_pkl(goal_save_location, goal_dict)
    window = ahk.active_window
    x = window.position[0]
    y = window.position[1]
    h = window.height
    w = window.width
    im = ImageGrab.grab(bbox=(
        x + c.SCREEN_SHOT_SIDES_REMOVAL_SIZE_4,
        y + c.SCREEN_SHOT_TOP_REMOVAL_SIZE_4,
        x + w - c.SCREEN_SHOT_SIDES_REMOVAL_SIZE_4,
        y + h - c.SCREEN_SHOT_SIDES_REMOVAL_SIZE_4
        ))
    location = os.path.join(c.IMAGE_OUTPUT_DIR, "screenshot" + str(counter) + ".png")
    im.save(location)
    imgMan.crop(im, c.GRIDS_OUTPUT_PATH, counter)
    counter += 1
    return

delay = c.DELAY_AFTER_MOVEMENT
looping = True
clearing_text = False
clearing_text_start_time = None
clearing_text_buffer_time = 4 # how long to wait before setting clearing text back to false
time.sleep(0.25) # don't record the white flash of the game loading screen
control.save_game(game_state_bank) # save the game on entry (very useful for new games)
while looping:
    screenshot()

    scsht = counter - 1
    if scsht < 0:
        scsht = 19
    try:
        # what is the current goal?
        current_goal = str(goal_dict[c.GOALS_KEY][0])
        # what tiles exist on the screen right now?
        # convert the cropped 'grids' into a prediction of what type of tile exists there
        grids_dir = os.path.join( c.GRIDS_OUTPUT_PATH, str(scsht) )
        tiles_dict = predict.predict_tiles_in_dir(grids_dir, GRIDS_MODEL, GRIDS_LABELS)
        print(f'Frame: {str(counter)} ... Current Goal: {current_goal} ...', end='', flush=True)
        if pathing.has_text_box(tiles_dict=tiles_dict):
            print('Clearing text...', flush=True)
            control.clear_text_sequence()
            if clearing_text:
                clearing_text_start_time = time.time()
            time.sleep(delay)
            continue
        if current_goal in c.DIRECTIONS:
            print(f'Moving {current_goal}...', flush=True)
            control.press_str_direction(current_goal)
            pathing.get_new_goal(goal_dict)
            time.sleep(delay)
        elif current_goal.startswith('a'):
            print('Pressing A...', flush=True)
            control.press_a(int(current_goal.split(':')[1]))
            pathing.get_new_goal(goal_dict)
            time.sleep(delay)
        elif current_goal == 'b':
            print('Pressing B...', flush=True)
            control.press_b(int(current_goal.split(':')[1]))
            pathing.get_new_goal(goal_dict)
            time.sleep(delay)
        elif current_goal == 'clear_text':
            if not clearing_text:
                clearing_text = True
                clearing_text_start_time = time.time()
            elif time.time() >= clearing_text_start_time + clearing_text_buffer_time:
                clearing_text = False
                pathing.get_new_goal(goal_dict)
            continue
    except Exception:
        pass # TODO this exception catches the out of bounds error on grabbing the current goal (should be fixed by get_new_goal)
    # if we aren't doing some basic action, try to find a path to the current goal
    path_to_goal = pathing.find_path_to_goal(tiles_dict, goal_dict)
    print("path to goal:", path_to_goal)
    if path_to_goal is None or len(path_to_goal) <= 0:
        print('Moving randomly to pathable tile...', flush=True)
        control.random_available_dir_world(pathing.available_moves_1_tile(tiles_dict, goal_dict))
        time.sleep(delay)
    else:
        print('Moving towards goal...', flush=True)
        for direction in path_to_goal:
            control.press_str_direction(direction)
            time.sleep(delay)
        pathing.get_new_goal(goal_dict)
# except KeyboardInterrupt as e:
#     print("Waiting for screenshot thread to close...")
#     daemon.join(timeout=10)
#     print("exiting..")
