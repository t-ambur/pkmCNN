import os
#
import constants as c
from util_fn import load_from_pkl

def is_pathable(label_name: str, grid_coord: str = None, previous_goal: str = '') -> bool:
    """
    Returns True if 'label_name' param is a pathable tile.
    :param label_name: the name of the 'tile', e.g. 'stairs_down'
    :param grid_coord: the grid coordinate being evaluated (not tile/label). Provide the player's grid to make the player pathable.
    :param previous_goal: the label that was the destination for the previous goal. Provide to block the AI from returning to that tile.
    """
    # return True for pathing starting from the player
    if grid_coord == c.PLAYER_GRID:
        return True
    # return False for previously found goal to block tiles of that type
    elif previous_goal == label_name:
        return False
    # tiles that return True
    pathable_tiles = ["floor_house", "floor_oaks_lab", "stairs_down", "stairs_up", "exit_rug", "rocky_soil",
        "grass", "grass_lonely", "grass_white", "grass_tall", "flower", "chair", "house_door", "oaks_table"]
    if label_name in pathable_tiles:
        return True
    return False


def convert_coordinate_to_integers(coordinate: str):
    x = int( coordinate[ 1 : coordinate.find('y') ] )
    y = int( coordinate[ coordinate.find('y') + 1 : len(coordinate) ] )
    return x, y


def get_neighbors_from_int_coordinates(x, y):
    up = 'x' + str(x) + 'y' + str(y-1)
    right = 'x' + str(x+1) + 'y' + str(y)
    down = 'x' + str(x) + 'y' + str(y+1)
    left = 'x' + str(x-1) + 'y' + str(y)
    return [up, right, down, left]


def create_pathing_tree(tiles_dict: dict, goal_dict: dict):
    pathing_tree = dict()
    goal_grid = None
    goal_tile = goal_dict[c.GOALS_KEY][0]
    previous_goal_tile = ''
    key: str
    for key, value in tiles_dict.items():
        if value == goal_tile:
            goal_grid = key
            if value != goal_dict[c.BLOCK_GOAL_KEY]:
                previous_goal_tile = goal_dict[c.BLOCK_GOAL_KEY]
        pathing_tree[key] = []
        # if this particular tile is unpathable, no reason to calculate its neighbors values
        if not is_pathable(value, grid_coord=key, previous_goal=previous_goal_tile):
            continue
        # the x and y values for this grid as integers
        x, y = convert_coordinate_to_integers(key)
        # the other keys that could exist around this grid
        directions = get_neighbors_from_int_coordinates(x, y)
        # for each coordinate_neighbor, see if it is a valid key
        for coordinate_neighbor in directions:
            if coordinate_neighbor in tiles_dict:
                # return True if this is the goal
                if coordinate_neighbor == goal_grid:
                    pathing_tree[key].append(coordinate_neighbor)
                # see if the key is pathable
                elif is_pathable(tiles_dict[key], grid_coord=key, previous_goal=previous_goal_tile):
                    # add to the tree if pathable
                    pathing_tree[key].append(coordinate_neighbor)
    return pathing_tree, goal_grid


def bfs(graph: dict, start_node: str, goal_location: str):
    visited = set()
    to_visit = [ (start_node, []) ]

    while len(to_visit) > 0:
        node, path = to_visit.pop(0)
        visited.add(node)
        # print('current node:', node)
        # print('current path:', path)
        for neighbor in graph[node]:
            if neighbor == goal_location:
                return path + [goal_location]
            else:
                if neighbor not in visited:
                    visited.add(neighbor)
                    to_visit.append( (neighbor, path + [neighbor]) )
    return []


def convert_path_to_directions(path: list, start_grid: str):
    directions = []
    current_x, current_y = convert_coordinate_to_integers(start_grid)
    for coordinate in path:
        next_x, next_y = convert_coordinate_to_integers(coordinate)
        # add the direction we need to go to the list
        if next_x > current_x:
            directions.append('right')
        elif next_x < current_x:
            directions.append('left')
        elif next_y > current_y:
            directions.append('down')
        elif next_y < current_y:
            directions.append('up')
        # update the current coordinates
        current_x = next_x
        current_y = next_y
    return directions


def find_path_to_goal(tiles_dict: dict, goal_dict: dict):
    """
    Constructs a tree of pathable nodes based on the predicted tiles.
    Traverses the tree with a path finding algorithm to find the path to the goal tile.

    Returns None if there are no goalTiles to evaluate.
    Returns None if the goal can't be found in this image.
    """
    # we've run out of goal tiles to evaluate
    if len(goal_dict[c.GOALS_KEY]) <= 0:
        return None
    pathing_tree, goal_location = create_pathing_tree(tiles_dict, goal_dict)
    # print("pathing_tree:", pathing_tree)
    # there is no goal tile visable
    if goal_location is None:
        return None
    print('goal location is:', goal_location)
    path = bfs(pathing_tree, c.PLAYER_GRID, goal_location)
    print('grids to the goal:', path)
    for grid in path:
        print(f'{tiles_dict[grid]}, ', end='')
    print()
    return convert_path_to_directions(path, c.PLAYER_GRID)


def available_moves_1_tile(tiles_dict: dict, goal_dict: dict):
    return {
        "up": is_pathable(tiles_dict['x4y3'], previous_goal=goal_dict[c.BLOCK_GOAL_KEY]),
        "left": is_pathable(tiles_dict['x3y4'], previous_goal=goal_dict[c.BLOCK_GOAL_KEY]),
        "right": is_pathable(tiles_dict['x5y4'], previous_goal=goal_dict[c.BLOCK_GOAL_KEY]),
        "down": is_pathable(tiles_dict['x4y5'], previous_goal=goal_dict[c.BLOCK_GOAL_KEY])
    }


# TODO implement JSON files for each stage (load new tile list from JSON file with stage name)
def load_goal_file(path: str):
    """
    Returns the dictionary saved at the given path (in a pkl file) or a 'new game' one.
    """
    goal_dict = dict()
    if os.path.exists(path):
        goal_dict = load_from_pkl(path)
    else:
        goal_dict[c.GOAL_STAGE_KEY] = 'newgame' # should align with a future JSON file containing all the goals for this stage
        goal_dict[c.GOALS_KEY] = ['stairs_down', 'exit_rug', 'down', 'block:house_door', 'grass_tall', 'pokedex', 'clear_text', 'pokeball', 'a:20', 'b:20']
        goal_dict[c.GOAL_COUNTER] = 0
        goal_dict[c.BLOCK_GOAL_KEY] = ''
    return goal_dict


def assign_goal_to_block(goal_dict: dict):
    """
    Assigns a tile to prevent pathing on based on the previous goal. If the next goal starts with 'block:',
    will block the tile requested and return True. Else returns False.
    """
    # remove the current goal
    previous_goal = goal_dict[c.GOALS_KEY].pop(0)
    # if the new goal is to block something
    if len(goal_dict[c.GOALS_KEY]) > 0 and str(goal_dict[c.GOALS_KEY][0]).startswith('block:'):
        goal_dict[c.BLOCK_GOAL_KEY] = str(goal_dict[c.GOALS_KEY][0]).split(':')[1]
        goal_dict[c.GOALS_KEY].pop(0)
        return True
    # flip stairs to block going back up them (on previous goal)
    elif previous_goal == 'stairs_down':
        goal_dict[c.BLOCK_GOAL_KEY] = 'stairs_up'
    elif previous_goal == 'stairs_up':
        goal_dict[c.BLOCK_GOAL_KEY] = 'stairs_down'
    else:
        goal_dict[c.BLOCK_GOAL_KEY] = ''
    return False


def block_tile(goal_dict: dict, tile: str):
    goal_dict[c.BLOCK_GOAL_KEY] = tile


def get_new_goal(goal_dict: dict):
    if len(goal_dict[c.GOALS_KEY]) > 0:
        blocker_tile_popped = assign_goal_to_block(goal_dict)
        # increment our current goal progress (based on how many goals were progressed)
        if blocker_tile_popped:
            goal_dict[c.GOAL_COUNTER] += 2
        else:
            goal_dict[c.GOAL_COUNTER] += 1
    # TODO load the next json file 'stage' when the goal list length reaches zero


def has_text_box(tiles_dict: dict):
    if tiles_dict[c.TEXTBOX_GRID] == 'textbox_ul_x0_y6':
        return True
    return False


# deprecated to avoid duplicate loops
# def find_goal_tile(tiles_dict: dict, goal_tile: str):
#     for key, value in tiles_dict.items():
#         if value == goal_tile:
#             return key
#     return None
