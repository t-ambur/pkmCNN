import argparse

parser = argparse.ArgumentParser(
                    prog = 'PokemonBlueCNN',
                    description = 'Uses a CNN and AHK to play Pokemon Blue without accessing internal game state',
                    epilog = 'Author: Trevor Amburgey')
# cli args
parser.add_argument('-c', '--continueGame', action='store_true', help="Provide this flag to load a savestate from the selected --saveGameStateBank")
parser.add_argument('-s', '--saveGameStateBank', default=1, help="Select an alternate save game bank to save and load from (default=1 for F1)")
parser.add_argument('-d', '--debugMode', action='store_true') # removes any existing goal file before continue
#
args = parser.parse_args()
